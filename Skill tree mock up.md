<<https://www.nasa.gov/mission_pages/station/research/experiments/explorer/Investigation.html?#id=7537>> Behavioral Core measures (for NASA astronauts)

Isolation and Depression

The skill trees that the therapist will activate and customize include social support and Depression.

Social Support ---> Building community ---> Identify Natural supports ----> FRANCY exercise (can name all people that a person knows then do sorting game where the individual places them in one of the categories (use color coding for Juan's willingness to access the support) ---> social interaction group (user group that allows chat function and playing assigned games in group format)

Depression ---> Seasonal Depression ---> Learning about Seasonal Depression

**coping skills** **--->** behavioral activation (game teaches about how to utilize the skill and role play, such as exercise etc.)

**coping skills ---**> self care plan (game explores client interests such as outdoor activities or meditation, client picks top three and completes role play or activities with guidance of therapist (accessible later or assigned as homework as well)

**coping skills ---**> Identifying negative thoughts with CBT triangle (walk through model with therapist first, then go through in game role play and process with therapist assistance.

The therapist and other providers would work together with Juan to identify the most pressing and present challenges Juan is experiencing. With this information, they would use the gaming platform to create a skill tree, where Juan would work with the different providers to learn skills, as well as complete assigned homework, or invite his friends and family to join him on the learning platform to help increase his connections with others as well as give his supports information and skill building to better support Juan.