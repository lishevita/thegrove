# TheGrove

A project created as part of the Isolation Solution Challenge at the Covid-19 focused Space Apps Challenge 2020.

A virtual game environment for mental and behavioral health support, where therapists and counselors can work directly with clients. 

## Summary
During the Covid-19 pandemic, we've seen that many people have found comfort and community in multiplayer games including the highly popular Animal Crossing. We've also seen that individuals accessing the mental health system have challenges interacting effectively with telehealth. Our hope is to create a dynamic system that simulates pre-Covid social interaction while providing the therapeutic benefits of role play and skill building in a game environment.

## Therapeutic Process
While the setting of The Grove is a game environment, the activity in The Grove has a very serious purpose and uses evidence based modalities. Skill building is an intregal part of mental health treatment and can be easily adapted in the game setting. In addition, the virtual environment gives users the ability to talk directly with licensed therapists and trained counselors without the need to go into an office, extending the potentials of telehealth provision.

The use of role playing games has been studied[^1] and is used today in several mental health agencies that work with adolescents today. Our software takes this to another level by building experiences into the game that can be assigned to users based on their needs and goals. Users can interact with these therapeutic activities on their own, together with a therapist or counselor, or with other supports. 

Based on the role that multiplayer open world games play in the lives of their fans, we believe that this environment can also provide needed social interaction with Natural and Informal supports for people in isolation due to distance, illness, or other factors. This social interaction can be free-play or conversational, or can include specific tasks linked to therapeutic goals.

In the User Stories included in this repository, we have explored the potential experience of a scientist who is living in Antarctica who is using this software as part of a wraparound mental health support program. Due to the limited time during the Space Apps Challenge weekend, we chose to create these vignettes to show the full potential of the program rather than focusing on user stories in other therapy contexts. However, the flexibility of the software is such that the multiplayer aspects highlighted in these stories in not necessary if the therapeutic framework of the agency does not involve Natural or Informal supports in their program. In the coming weeks, we will be adding additional scenarios to show how variations in how the software might be used.

### User Stories
* [Juan, the scientist](User\ Story1\ -\ Juan.md)
* [Julia, the therapist](User\ Story2\ -\ Julia.md)
* [Nacho, the care coordinator](User\ Story3\ Nacho.md)
* [Juan's Family](User\ story4\ -\ Juans\ Family.md)

### Skill Tree Examples

![Depression Management Skill Tree](Depression-management-skill-tree.svg)

To understand how the Skill trees work, explore these examples:
* [Skill Tree Mockup, text](Skill\ tree\ mock up.md)
* [Skill Tree Walkthrough](Skill\ tree\ walkthrough.md)


[^1]: - Gutierrez, Raul, ["Therapy & Dragons: A look into the Possible Applications of Table Top Role Playing
Games in Therapy with Adolescents"](https://scholarworks.lib.csusb.edu/cgi/viewcontent.cgi?referer=https://www.google.com/&httpsredir=1&article=1609&context=etd) (2017). Electronic Theses, Projects, and Dissertations. 527.
https://scholarworks.lib.csusb.edu/etd/527 
      - McConnaughey, Adam. (2015). [Playing to Find Out: Adapting Story Games for Group Therapy with Teens.](https://sophia.stkate.edu/cgi/viewcontent.cgi?article=1492&context=msw_papers)
Retrieved from Sophia, the St. Catherine University repository website: https://sophia.stkate.edu/
msw_papers/488 
      - Additional research at https://www2.rpgresearch.com/about/about-rpg-research
      - [Hawkes-Robinson, William. (2013). Role-Playing Gaming Recreation Therapy Handbook of Practice Hypothetical Draft RFC..](https://www.researchgate.net/publication/237074830_Role-Playing_Gaming_Recreation_Therapy_Handbook_of_Practice_Hypothetical_Draft_RFC)

## Architectural Information
The software will be built so that there is a game client that runs on user machines, an agency server, and a Google Voice Builder (GBV) running in the cloud.

Game clients hold the main logic of the game and allow users to interact in the world, communicate with other users, customize their own avatars, and create the training data that gets sent to GBV for creating voice configurations. Clients send the Agency Server information about games completed ("scores"), time in game, and other relevant data from inside the game needed for tracking therapeutic progress.

The Agency Server holds encrypted user data, additional terrains and games that can be sent to the game clients, and contains video trainings and other files that may be sent to a game client for viewing or usage. The Agency Server also handles communication between multiple clients when they are playing together (ie not one-to-one).

The Google Voice Builder is on a separate cloud service. It receives voice training files from clients and sends the resulting voice configurations to the Agency Server which can then send those configurations to users who have permission to "hear" that user's customized voice.

The main game will be created with [Uchronia Project Blender Game Engine (UPBGE)](https://upbge-docs.readthedocs.io/en/latest/manual/introduction/briefing.html) and Python. Some additional logic may be built in C++ to extend the abilities of UPBGE and to ensure that all security and privacy needs to meet or exceed HIPAA compliance is in place.

For more information see 
* [Notes on Game Architecture](Notes\ on\ Game\ Architecture.md)
![Architecture 1 diagram](Architecture1.png)

## Integrating NASA Research ##
This work integrates NASA Research in a number of ways. We are integrating mental health and behavioral assessments from NASA's research directly into the program. We are also using concepts that have been explored in research projects conducted by NASA. For further information, view our page on [Integrating Research](Integrating Research.md).

## The Team ##
### Laura Hughes, LMHCA MHP ###
Laura is a therapist specializing in families, children and adolescents. She has 6 years of experience in Indiana and Washington states. She has worked with underserved populations and completed research in animal assisted therapy at Washington State University. For the past 3 years she has worked in Washington State's [WISe program](https://www.hca.wa.gov/health-care-services-supports/behavioral-health-recovery/wraparound-intensive-services-wise), providing intensive wraparound services to families and children.

### Noelia Gonzalez Rodriguez ###
Noelia is a professional Data Scientist & AI developer, since time immemorial. She is currently Manager Data Analytics at Electronic Arts (EA). She has more than 20 years of experience helping organizations, national and international, to improve business decisions through the use of quantitative methods applied to data and through technology. She has participated in more than 500 projects in different sectors (Banking, Insurance, Retail, Utilities, Telco, AAPP, …). Previously she was a director in the areas of data and technology analytics, as well as collaborating with different educational institutions and universities.

In addition, she serves as a Professor at the University Isabel I. She was Business Mentor in the Master Data Science Bootcamp of the Instituto de Empresa (IE).
Degree in Statistics by University of Valladolid (Spain). Master in Market Research and Information Systems by UNED (Spain). 

### Noé Martinez ###
Noé is an artist who creates graphics and animations in Blender, the free and open-source 3D computer graphics software toolset. He holds an AA in Viticulture and Enology from Walla Walla Community College and a BA in Rhetoric from Washington State University.

### Lisha Sterling ###
Lisha is the executive director of Geeks Without Bounds. She has a lifetime of experience in software development, systems administration, and technical project management. For the past ten years she has specialized in open source humanitarian software. This past year she took a sabatical from her day job at Geeks Without Bounds to work at a mental health agency as a Certified Peer Counselor in the WISe program. She brings that experience back to this project. 


